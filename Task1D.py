
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list


def run():
    station_list = build_station_list()
    rivers = rivers_with_station(station_list)

    river_dictionary = stations_by_river(station_list)

    #ordering the list in alphbetical order
    rivers.sort()

    #rivers with at least one monitering station
    print(len(rivers), "//")
    #first 10 rivers names in alphabetical order
    print(rivers[0:10], "//")

    objects1 = river_dictionary['River Aire']
    name_list1 = []
    for i in range(len(objects1)):
        name_list1.append(objects1[i].name)
    print(name_list1, "//")
    
    objects2 = river_dictionary['River Cam']
    name_list2 = []
    for i in range(len(objects2)):
        name_list2.append(objects2[i].name)
    print(name_list2, "//")

    objects3 = river_dictionary['River Thames']
    name_list3 = []
    for i in range(len(objects3)):
        name_list3.append(objects3[i].name)
    print(name_list3 )
    

if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()