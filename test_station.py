# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.geo import rivers_by_station_number
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_level_over_threshold
from floodsystem.flood import stations_highest_rel_level



def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_typical_range_consistent():
    s_id = 'test-s-id'
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange_consistent = (-2.3, 3.4445)
    trange_inconsistent = (-2.3, -3.4445)
    river = "River X"
    town = "My Town"
    s_consistent = MonitoringStation(s_id, m_id, label, coord, trange_consistent, river, town)
    s_inconsistent = MonitoringStation(s_id, m_id, label, coord, trange_inconsistent, river, town)
    assert MonitoringStation.typical_range_consistent(s_consistent) == True
    assert MonitoringStation.typical_range_consistent(s_inconsistent) == False


def test_inconsistent_typical_range_stations():
    s_id = 'test-s-id'
    m_id = "test-m-id"
    inconsistent_label = "inconsistent"
    consistent_label = 'consistent'
    coord = (-2.0, 4.0)
    trange_consistent = (-2.3, 3.4445)
    trange_inconsistent = (-2.3, -3.4445)
    river = "River X"
    town = "My Town"
    s_consistent = MonitoringStation(s_id, m_id, consistent_label, coord, trange_consistent, river, town)
    s_inconsistent = MonitoringStation(s_id, m_id, inconsistent_label, coord, trange_inconsistent, river, town)
    test_list = [s_consistent,s_inconsistent]
    assert inconsistent_typical_range_stations(test_list) == [s_inconsistent]


def test_relative_water_level():

    stations = build_station_list()

    update_water_levels(stations)

    all_levels = []

    for i in range(len(stations)):
        current_rel_level = MonitoringStation.relative_water_level(stations[i])
        
        if current_rel_level == None :
            pass
        
        else:
            all_levels.append(current_rel_level)

    for i in range(len(all_levels)):
        assert type(all_levels[i]) == float