from floodsystem.plot import plot_water_level_with_fit
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
import numpy as np
from floodsystem.utils import sorted_by_key
import datetime

def run():
    stations = build_station_list()
    update_water_levels(stations)
    stations_tuple_list = []
    for i in range(len(stations)):
        if type(stations[i].relative_water_level()) == float:
            stations_tuple_list.append((stations[i],stations[i].relative_water_level()))
    stations_tuple_list_sorted = sorted_by_key(stations_tuple_list, 1)
    top_5 = stations_tuple_list_sorted[-5:]
    for i in range(len(top_5)):
        dates, levels = fetch_measure_levels(top_5[i][0].measure_id, dt=datetime.timedelta(days=2))
        plot_water_level_with_fit(top_5[i][0], dates, levels, 4)


if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()