from floodsystem.plot import plot_water_level_with_prediction
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.analysis import prediction, predicted_relative
from floodsystem.utils import sorted_by_key
from floodsystem.datafetcher import fetch_measure_levels
import datetime
import matplotlib
import pytest

def run():
    #build a list of stations
    stations = build_station_list()
    station_sort_list = []

    #for every station, if no dodgy data, add tuple of station, prediction to list
    for i in range(len(stations)):
        #measure id valid
        if type(stations[i].measure_id) == str:
            dates, levels = fetch_measure_levels(stations[i].measure_id, dt=datetime.timedelta(days=2))
        else:
            pass
        #levels valid
        valid_levels = True
        for j in range(len(levels)):
            if type(levels[j]) != float:
                valid_levels = False
        #dates valid
        if len(dates) != 0 and len(levels) == len(dates) and type(levels) == list and valid_levels:
            try:
                current_prediction = predicted_relative(stations[i],dates, levels, 1)
                warning = None
                if current_prediction <= 0.9:
                    pass

                elif 0.9 < current_prediction <= 1.5:
                    warning = 'low'

                elif 1.5 < current_prediction <= 2:
                    warning = 'moderate'

                elif 2 < current_prediction <= 5:
                    warning = 'high'

                else:
                    warning = 'severe'
                
                station_sort_list.append((stations[i], current_prediction, warning))
            except ValueError:
                pass
        #print progress in %
        print('Progress = {}%'.format(round(100*i/len(stations),3)))

    #Sort list
    sorted_station_list = sorted_by_key(station_sort_list, 1, True)
    top_10 = sorted_station_list[:10]
    print(top_10)
    #plot prediction graph for top 3 stations
    for i in range(len(top_10)):
        plot_water_level_with_prediction(top_10[i][0],1)

if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()