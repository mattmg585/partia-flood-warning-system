'''Demonstration program for task 1F - producing a list of inconsistent stations'''

from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

def run():
    station_list = build_station_list()
    inconsistent_list = inconsistent_typical_range_stations(station_list)
    inconsistent_names = []
    for i in range(len(inconsistent_list)):
        inconsistent_names.append(inconsistent_list[i].name)
    inconsistent_names.sort()
    print(inconsistent_names)


if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()
