from floodsystem.plot import plot_water_level_with_fit, plot_water_levels
import pytest
from floodsystem.station import MonitoringStation


def test_plot_water_levels():
    s_id = "test-s-id"
    m_id = "test-m-id"
    non_string_label = 2.4
    valid_label = 'Test Station'
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s_non_string = MonitoringStation(s_id, m_id, non_string_label, coord, trange, river, town)
    s_valid = MonitoringStation(s_id, m_id, valid_label, coord, trange, river, town)
    dates_true, levels_true = [0,1,2] , [0.23, 0.26, 0.59]
    dates_false, levels_false = [0,1,2], [0.23, 0.26, 0.59, 0.69420]

    with pytest.raises(TypeError):
        plot_water_levels(s_non_string, dates_true, levels_true)
    
    with pytest.raises(ValueError):
        plot_water_levels(s_valid, dates_false, levels_false)


def test_plot_water_level_with_fit():
    s_id = "test-s-id"
    m_id = "test-m-id"
    non_string_label = 2.4
    valid_label = 'Test Station'
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s_non_string = MonitoringStation(s_id, m_id, non_string_label, coord, trange, river, town)
    s_valid = MonitoringStation(s_id, m_id, valid_label, coord, trange, river, town)
    dates_true, levels_true = [0,1,2] , [0.23, 0.26, 0.59]
    dates_false, levels_false = [0,1,2], [0.23, 0.26, 0.59, 0.69420]
    
    with pytest.raises(TypeError):
        plot_water_level_with_fit(s_non_string, dates_true, levels_true, 2)
    
    with pytest.raises(ValueError):
        plot_water_level_with_fit(s_valid, dates_false, levels_false, 2)
    
    with pytest.raises(ValueError):
        plot_water_level_with_fit(s_valid, dates_true, levels_true, -2.4)
    