from floodsystem.geo import stations_by_distance
from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list
from haversine import haversine, Unit
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number


def test_stations_by_distance():
    test_list = build_station_list()
    output = stations_by_distance(test_list,(52.2053,0.1218))
    for i in range(len(output)-1):
        assert output[i][1] <= output[i+1][1]
        assert output[i][1] > 0
    assert len(output)==len(test_list)
  
def test_stations_within_radius():
    test_list = build_station_list()
    output = stations_within_radius(test_list, (52.2053,0.1218), 10)
    for i in range(len(output)):
        assert haversine(output[i].coord,(52.2053,0.1218)) <= 10

#testing to make sure there are not more rivers than objects
def test_rivers_with_station():
    test_list = build_station_list()
    output = rivers_with_station(test_list)
    assert len(test_list) >= len(output)

#testing that the sum of all the objects in the dictionary is equal to the total number of objects
def test_stations_by_river():
    test_list = build_station_list()
    output = stations_by_river(test_list)

    assert len(output) == len(rivers_with_station(test_list))

    
    
        
   





