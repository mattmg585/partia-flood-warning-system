from floodsystem.analysis import polyfit
import numpy as np
import datetime
import matplotlib
import pytest

def test_analysis():
    dates = [datetime.datetime(2020, 2, 21, 12), datetime.datetime(2020, 2, 22, 12)]
    levels = [2,4]
    assert np.round_(polyfit(dates, levels, 1)[0].c,10).all() == np.round_(np.poly1d([2.,2.]).c,10).all()
    assert polyfit(dates, levels, 1)[1] == matplotlib.dates.date2num(datetime.datetime(2020, 2, 21, 12))
    levels_error = [2,4,5]
    with pytest.raises(ValueError):
        polyfit(dates, levels_error, 1)