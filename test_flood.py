from floodsystem.geo import rivers_by_station_number
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_level_over_threshold
from floodsystem.flood import stations_highest_rel_level

stations = build_station_list()

update_water_levels(stations)

def test_stations_level_over_threshold():

    h = stations_level_over_threshold( stations, 0.5)        

    assert h[len(h)-1][1] > 0.5


#def test_stations_highest_rel_level():

