'''Demonstration program for Task 1B : prints a list of tuples (station name, town, distance) for 10 closest and 10 furthest stations from cambridge city centre'''


from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def run():
    station_list = build_station_list()
    cambs_city_center = (52.2053,0.1218)
    ordered_station_list = stations_by_distance(station_list, cambs_city_center)
    output = []
    for i in range(len(ordered_station_list)):
        output.append((ordered_station_list[i][0].name, ordered_station_list[i][0].town, ordered_station_list[i][1]))
    print('Closest 10 Stations: ',output[:9])
    print('Furthest 10 Stations: ',output[-10:])


if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()