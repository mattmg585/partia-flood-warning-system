from floodsystem.geo import rivers_by_station_number
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_level_over_threshold
from floodsystem.flood import stations_highest_rel_level


def run():
    
    stations = build_station_list()

    update_water_levels(stations)

    total_list = (stations_highest_rel_level(stations, (len(stations))))  
    
    final_list = []

    for i in range(len(total_list)):        
                  
        level = total_list[i][1]

        if level <= 0.9:
            pass

        elif 0.9 < level <= 1.5:
            warning = 'low'

        elif 1.5 < level <= 2:
            warning = 'moderate'

        elif 2 < level <= 5:
            warning = 'high'

        else:
            warning = 'severe'

        for j in range(len(total_list)):
            current_station = total_list[i][0]

            if (stations[j].name == current_station) and (level > 0.9):
                final_list.append((warning , level , stations[j].name))

    print(final_list)
         
    


    



if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()

