from floodsystem.geo import rivers_by_station_number
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_level_over_threshold


def run():
    
    large_list = []
    final_list = []

    stations = build_station_list()

    update_water_levels(stations)


    large_list = stations_level_over_threshold(stations, 0.8)

    for i in range(len(large_list)):
        current_name = large_list[i][0].name
        current_level = large_list[i][1]
        final_list.append( (current_name , current_level) ) 
    
    for i in range(len(final_list)):
        print(final_list[i])

if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()