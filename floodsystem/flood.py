from floodsystem.station import MonitoringStation
from floodsystem.geo import rivers_by_station_number
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels


def stations_level_over_threshold(stations, tol):

    empty_list = []
    level_list = []
    final_list = []
    

    for i in range(len(stations)):
        
        current_object = stations[i]
        
        

        # set a variable equal to the relative level of the current object
        relative_level = (MonitoringStation.relative_water_level(current_object))
        
        #can't do calculations with a none type so if it is, then move on
        if relative_level == None :
            pass
        #otherwise if the relative level is greater than the tolerence:
        elif relative_level > tol:
                                                    
            #add all the relative levels > tol to their own list
            level_list.append(relative_level)
            #make a mini list with [station object, that station objects relative level]
            mini_list = [ current_object , relative_level ]
            #make a list of all the small lists
            empty_list.append(mini_list)
        # sort the list of relative level > tol so that they're in decreasing size order
        level_list.sort( reverse = True )
     
    for i in range(len(level_list)):
        current_level = level_list[i]
        for j in range(len(level_list)):

            if empty_list[j][1] == current_level:
                final_list.append( (empty_list[j][0] , current_level) )
    
    return final_list



def stations_highest_rel_level(stations, N):

    stations = build_station_list()

    update_water_levels(stations)

    all_levels = []

    for i in range(len(stations)):
        current_rel_level = MonitoringStation.relative_water_level(stations[i])
        
        if current_rel_level == None :
            pass
        
        else:
            all_levels.append(current_rel_level)
    
    all_levels.sort( reverse = True )

    wanted_levels = all_levels[:N]

    final_list = []

    for i in range(len(wanted_levels)):
        current_rel_level = wanted_levels[i]
        for j in range(len(stations)):

            if MonitoringStation.relative_water_level(stations[j]) == current_rel_level:
                final_list.append( (stations[i].name , current_rel_level) )

        

    return final_list