import matplotlib.pyplot as plt
import matplotlib
import datetime
import numpy as np
import pytest
import floodsystem.station


def polyfit(dates, levels, p):
    '''Outputs tuple of (polynomial trendline - type = numpy.poly1D, t axis offset)'''
    if len(dates) != len(levels):
        raise ValueError('dates and levels must be lists of the same length')
    t = matplotlib.dates.date2num(dates)
    t_translated = t - t[0]
    try:
        p_coeff = np.polyfit(t_translated, levels, p)
        poly = np.poly1d(p_coeff)
    except np.linalg.LinAlgError:
        raise ValueError('data invalid - np.polyfit cannot generate fit line')
    return (poly, t[0])


def prediction(dates, levels, p, t=12):
    '''returns water level at time t hours from current, under polynomial fit order p'''
    if len(dates) != len(levels):
        raise ValueError("dates and levels must be the same length")
    try:
        poly = polyfit(dates, levels, p)[0]
        poly_offset = polyfit(dates, levels, p)[1]
    except ValueError:
        raise ValueError('polyfit failed due to poor data')
    delta_t = t/24
    return poly(matplotlib.dates.date2num(dates[0]) - poly_offset + delta_t)

def predicted_relative(station, dates, levels, p=4, t=12):
    predicted_level = prediction(dates, levels, p, t)
    ratio = 0
    if not station.typical_range_consistent():
        raise ValueError('typical range inconsistent')
    else:
        ratio = (predicted_level - station.typical_range[0])/(station.typical_range[1] - station.typical_range[0])
    return ratio