#Copyright (C) 2018 Garth N. Wells

#SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from haversine import haversine, Unit
from .utils import sorted_by_key  # noqa
from floodsystem.station import MonitoringStation


def stations_by_distance(stations, p):
    '''Function returning a sorted list of tuples (station, distance from point p in km) 
    given a list of objects, stations and a coordinate, p'''
    assert len(p)==2
    if p[0] < -90 or p[0] > 90 or p[1] < -180 or p[1] > 180:
        raise ValueError("p coordinate invalid")
    station_list = []
    for i in range(len(stations)):
        if stations[i].coord[0] < -90 or stations[i].coord[0] > 90 or stations[i].coord[1] < -180 or stations[i].coord[1] > 180:
            raise ValueError("Invalid Coordinate for {}".format(stations[i].name))
        dist = haversine(p,stations[i].coord)
        if dist < 0 or dist > 20037.5:
            raise ValueError("Haversine function returned invalid distance - negative or >circumfrence of earth/2")
        station_list.append((stations[i],dist))
    return sorted_by_key(station_list,1)


def stations_within_radius(stations, centre, r):
    '''Function returning a sorted list of station objects within a radius 'r' from a point 'centre' '''
    if r < 0:
        raise ValueError("r<0 : r must be positive")
    assert len(centre)==2
    if centre[0] < -90 or centre[0] > 90 or centre[1] < -180 or centre[1] > 180:
        raise ValueError("centre coordinate invalid")
    sorted_station_list = stations_by_distance(stations, centre)
    stations_in_range = []
    for i in range(len(sorted_station_list)):
        if sorted_station_list[i][1] <= r:
            stations_in_range.append(sorted_station_list[i][0])
        else:
            break
    return stations_in_range



#function returning a list of the names of rivers which have a monitering station on (no repeated names)
def rivers_with_station(stations):
         
    rivers_with_stations = []

    for i in range(len(stations)):
        #adding the 6th entry in each object as this is the position of the river name
        if type(stations[i].river) != str:
            raise ValueError("river name must be a string")
        else:
            rivers_with_stations.append(stations[i].river)

    #removing all repeated river names
    rivers_with_stations_not_repeated = set(rivers_with_stations)

    river_names_list = list(rivers_with_stations_not_repeated)


    return river_names_list


#function returning a dictionary where the key is the rivername and the value is a list of the station objects
def stations_by_river(stations):

    dictionary_data = {}

    river_names = rivers_with_station(stations)
    
    
    for i in range(len(river_names)):
        current_river = river_names[i]
        current_object_list = []
        for j in range(len(stations)):
            if stations[j].river == current_river:
                current_object_list.append(stations[j])
            
        if len(current_object_list) == 0:
            raise ValueError( "the current river has no stations")
        else:
            dictionary_data[current_river] = current_object_list

    return dictionary_data


#function returning a list of tuples, each with the river name, and the number of stations on that river
def rivers_by_station_number(stations, N):
    
    if N < 0:
        raise ValueError("you can't get a negative value of a physical object")

    final_list = []
    
    #dictionary with rivernames and then its station objects
    station_dic = stations_by_river(stations)

    #list of river names
    river_names = rivers_with_station(stations)
    
    #lists to use in for loops
    current_length = []
    length_list = []
    
    for i in range(len(river_names)):
        current_river = river_names[i]
        current_river_objects = station_dic[current_river]
        current_length = len(current_river_objects)
        length_list.append(current_length)
    
    max_number = max(length_list)

    if max_number < 0:
        raise ValueError("a maximum number must be greater than or equal to 0 (in the case N>=0)") 
    counter = 0
    for i in range(max_number, 0 , -1):
        if counter < N:
            for j in range(len(river_names)):
                current_river = river_names[j]
                current_river_objects = station_dic[current_river]
                current_length = len(current_river_objects)
                if current_length == i:
                    current_tuple = (current_river , current_length)
                    final_list.append(current_tuple)
                    counter = counter + 1
        else:
            break
     
    
    return final_list