import matplotlib.pyplot as plt
from matplotlib.dates import date2num as d2n
from matplotlib.dates import num2date as n2d
import matplotlib
from datetime import datetime, timedelta
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import polyfit
import numpy as np
from floodsystem.analysis import prediction

def plot_water_levels(station, dates, levels):
    if len(dates) != len(levels):
        raise ValueError('dates and levels must be same length')
    if type(station.name) != str:
        raise TypeError('station name isn\'t a string') 
    plt.plot(dates, levels)
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    plt.show()


def plot_water_level_with_fit(station, dates, levels, p):
    if len(dates) != len(levels):
        raise ValueError('dates and levels must be same length')
    if type(station.name) != str:
        raise TypeError('station name isn\'t a string') 
    if p<0 or type(p) != int:
        raise ValueError('p must be a natural number')
    t = d2n(dates)
    trendline_eqn = polyfit(dates, levels, p)[0]
    trendline_offset = polyfit(dates, levels, p)[1]
    trendline = trendline_eqn(t-trendline_offset)
    plt.plot(dates,trendline)
    plt.plot(dates, levels)
    if station.typical_range_consistent():
        plt.plot(dates,station.typical_range[0]*np.ones(len(dates)))
        plt.plot(dates,station.typical_range[1]*np.ones(len(dates)))
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    plt.show()

def plot_water_level_with_prediction(station, p=4, forecast_t=12):
    #fetch water levels from last 2 days
    dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=2))
    #check stuff
    if len(dates) != len(levels):
        raise ValueError('dates and levels must be same length')
    if type(station.name) != str:
        raise TypeError('station name isn\'t a string') 
    if p<0 or type(p) != int:
        raise ValueError('p must be a natural number')
    delta_t = forecast_t/24
    t = d2n(dates)
    trendline_eqn = polyfit(dates, levels, p)[0]
    trendline_offset = polyfit(dates, levels, p)[1]
    trendline = trendline_eqn(t-trendline_offset)
    t_forecast = np.linspace(t[0], t[0] + delta_t, delta_t*24*10)
    trendline_forecast = trendline_eqn(t_forecast-trendline_offset)
    plt.plot(n2d(t_forecast), trendline_forecast, '--')
    plt.plot(dates,trendline)
    plt.plot(dates, levels)
    plt.plot(n2d(d2n(dates[0])+delta_t), prediction(dates, levels, p, forecast_t), 'x')
    if station.typical_range_consistent():
        plt.plot(dates,station.typical_range[0]*np.ones(len(dates)))
        plt.plot(dates,station.typical_range[1]*np.ones(len(dates)))
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    plt.show()