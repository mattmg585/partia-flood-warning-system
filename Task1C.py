'''Demonstration program for Task 1C - produces list of stations within 10km if Cambridge City Centre'''


from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    cambs_city_center = (52.2053,0.1218)
    station_list = build_station_list()
    stations_in_range = stations_within_radius(station_list, cambs_city_center, 10)
    name_list = []
    for i in range(len(stations_in_range)):
        name_list.append(stations_in_range[i].name)
    name_list.sort()
    print(name_list)


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()